﻿using UnityEngine;
using DLLtest;

public class wallter_backend : MonoBehaviour
{
    public int coin, crystal;
    private Wallter wallter = new Wallter();

    public void zero(string currency)
    {
        if (currency == "coin") coin = wallter.zero;
        else crystal = wallter.zero;
    }
    public void inkr(string currency)
    {
        if (currency == "coin") coin = wallter.ink(coin);
        else crystal = wallter.ink(crystal);
    }

    public void saveMoneyPP()
    {
        wallter.SavePP("coin", coin);
        wallter.SavePP("crystal", crystal);
    }
    public void loadMoneyPP()
    {
        coin = wallter.loadPP("coin");
        crystal = wallter.loadPP("crystal");
    }

    public void saveInF()
    {
        wallter.SaveInF(new int[] { coin, crystal });
    }
    public void loadInF()
    {
        coin = wallter.LoadInF()[0];
        crystal = wallter.LoadInF()[1];
    }

    public void SaveInFB()
    {
        wallter.SaveInFB(new int[] { coin, crystal });
    }
    public void LoadInFB()
    {
        coin = wallter.LoadInFB()[0];
        crystal = wallter.LoadInFB()[1];
    }
}