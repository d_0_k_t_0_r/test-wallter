﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public wallter_backend wallter_be;
    public Text coinT, crystalT;

    private void Update()
    {
        coinT.text = wallter_be.coin.ToString();
        crystalT.text = wallter_be.crystal.ToString();
    }

    public void zero(string currency)
    {
        wallter_be.zero(currency);
    }
    public void inkr(string currency)
    {
        wallter_be.inkr(currency);
    }

    public void savePP()
    {
        wallter_be.saveMoneyPP();
    }
    public void loadPP()
    {
        wallter_be.loadMoneyPP();
    }

    public void saveF()
    {
        wallter_be.saveInF();
    }
    public void loadF()
    {
        wallter_be.loadInF();
    }

    public void saveFB()
    {
        wallter_be.SaveInFB();
    }
    public void loadFB()
    {
        wallter_be.LoadInFB();
    }
}
